#include "game_logic.h"
#include "protocol.h"

#define M_PI 3.14159265358979323846
#define print(...) { std::cout << __VA_ARGS__ << std::endl; }

using namespace hwo_protocol;

unsigned int gTick;
bool speedTest = true;

game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
	  { "yourCar", &game_logic::on_yourCar },
	  { "gameInit", &game_logic::on_game_init },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
	  { "spawn", &game_logic::on_spawn },
      { "crash", &game_logic::on_crash },
	  { "lapFinished", &game_logic::on_lapFinished },
	  { "finish", &game_logic::on_finish },
	  { "tournamentEnd", &game_logic::on_tournamentEnd },
      { "gameEnd", &game_logic::on_game_end },
	  { "turboAvailable", &game_logic::on_turbo_available },
	  { "turboStart", &game_logic::on_turbo_start },
	  { "turboEnd", &game_logic::on_turbo_end },
      { "error", &game_logic::on_error }
    }
{
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, msg);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_yourCar(const jsoncons::json& data)
{
	m_Color = data["data"]["color"].as<std::string>();
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
	const auto& lanes = data["data"]["race"]["track"]["lanes"];

	for (unsigned int i = 0; i < lanes.size(); i++)
		m_Lanes.push_back(Lane(lanes[i]["index"].as<int>(), lanes[i]["distanceFromCenter"].as<double>()));

	bool wasSwap = true;
	while (wasSwap)
	{
		wasSwap = false;

		for (unsigned int i = 1; i < m_Lanes.size(); i++)
		{
			if (m_Lanes[i-1].m_DistanceFromCenter > m_Lanes[i].m_DistanceFromCenter)
			{
				std::swap(m_Lanes[i-1], m_Lanes[i]);
				wasSwap = true;
			}
		}
	}

	const auto& segments = data["data"]["race"]["track"]["pieces"];

	for (unsigned int i = 0; i < segments.size(); i++)
	{
		double radius = segments[i].has_member("radius") ? segments[i]["radius"].as<double>() : 0;
		double length = segments[i].has_member("length") ? segments[i]["length"].as<double>() : 0;
		double angle = segments[i].has_member("angle") ? segments[i]["angle"].as<double>() : 0;
		bool isSwitch = segments[i].has_member("switch") ? true : false;

		if (radius > 0)
			length = radius * M_PI / 180 * fabs(angle);
			
		m_TrackSegments.push_back(Segment(length, radius, angle, isSwitch));

		for (unsigned int j = 0; j < m_Lanes.size(); j++)
		{
			double correctedRadius;
			double correctedLength;

			if (radius > 0)
			{
				correctedRadius = radius + (m_Lanes[j].m_DistanceFromCenter * (angle < 0 ? 1 : -1));
				correctedLength = correctedRadius * M_PI / 180 * fabs(angle);
			}
			else
			{
				correctedLength = length;
			}

			m_TrackSegments.back().m_Lanes.push_back(LaneInfo(angle, correctedRadius, correctedLength));
		}
	}

	const auto& cars = data["data"]["race"]["cars"];

	double length = 0;
	double width = 0;
	double flagPosition = 0;

	for (unsigned int i = 0; i < cars.size(); i++)
	{
		if (length == 0 && width == 0 && flagPosition == 0)
		{
			m_Car.m_Length = cars[i]["dimensions"]["length"].as<double>();
			m_Car.m_Width = cars[i]["dimensions"]["width"].as<double>();
			m_Car.m_GuideFlagPosition = cars[i]["dimensions"]["guideFlagPosition"].as<double>();
		}

		const auto& color = cars[i]["id"]["color"].as<std::string>();
		m_Racers.push_back(Racer(color));
	}

	m_MassConst = m_TerminalConst = m_DragConst = -1;

	AnalyzeTrack();
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
	m_Throttle = 1.0;
	gTick = 0;
	m_SwitchSegment = -1;
	m_RacerToCopy = NULL;
	m_IndexToCopy = -1;

	for (unsigned int i = 0; i < m_Racers.size(); i++)
	{
		if (m_Racers[i].bendInfo.size() > 0 && m_Racers[i].bendInfo.back().lastTick == -1)
			m_Racers[i].bendInfo.pop_back();

		if (m_Racers[i].speedInfo.size() > 0)
			m_Racers[i].speedInfo.pop_back();
	}

	return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
	if (!data.has_member("gameTick"))
		return { make_ping() };

	gTick = data["gameTick"].as<int>();
	const auto& dat = data["data"];

	// process cars
	for (unsigned int i = 0; i < dat.size(); i++)
	{
		std::string color = dat[i]["id"]["color"].as<std::string>();
		Racer& racer = GetRacer(color);

		if (racer.speedInfo.size() > 0 && racer.speedInfo.back().crashed)
			continue;

		SpeedInfo2 speedInfo;
		
		speedInfo.currentAngle = dat[i]["angle"].as<double>();
		speedInfo.currentSegment = dat[i]["piecePosition"]["pieceIndex"].as<int>();
		speedInfo.currentDistance = dat[i]["piecePosition"]["inPieceDistance"].as<double>();
		speedInfo.currentLane = dat[i]["piecePosition"]["lane"]["endLaneIndex"].as<int>();
		speedInfo.changingLane = (dat[i]["piecePosition"]["lane"]["endLaneIndex"].as<int>() != dat[i]["piecePosition"]["lane"]["startLaneIndex"].as<int>());
		
		if (racer.color == m_Color)
			speedInfo.throttle = m_Throttle;

		if (racer.speedInfo.size() == 0)
		{
			speedInfo.currentSpeed = 0;
			speedInfo.hasTurbo = false;
			speedInfo.crashed = false;
		}
		else
		{
			if (speedInfo.currentSegment == racer.speedInfo.back().currentSegment)
			{
				speedInfo.currentSpeed = speedInfo.currentDistance - racer.speedInfo.back().currentDistance;
			}
			else
			{
				if (m_TrackSegments[racer.speedInfo.back().currentSegment].IsBend())
				{
					if (racer.bendInfo.size() > 0 && (racer.bendInfo.back().segmentIndex == racer.speedInfo.back().currentSegment))
						racer.bendInfo.back().lastTick = racer.speedInfo.size() - 1;
				}

				if (m_TrackSegments[speedInfo.currentSegment].IsBend())
				{
					BendInfo2 bendInfo;
					bendInfo.segmentIndex = speedInfo.currentSegment;
					bendInfo.firstTick = racer.speedInfo.size() - 1;
					bendInfo.lastTick = -1;

					racer.bendInfo.push_back(bendInfo);
				}

				speedInfo.currentSpeed = (m_TrackSegments[racer.speedInfo.back().currentSegment].m_Lanes[speedInfo.currentLane].length - racer.speedInfo.back().currentDistance) + speedInfo.currentDistance;
			}

			racer.speedInfo.back().deltaSpeed = speedInfo.currentSpeed - racer.speedInfo.back().currentSpeed;
			racer.speedInfo.back().deltaAngle = speedInfo.currentAngle - racer.speedInfo.back().currentAngle;
			speedInfo.hasTurbo = racer.speedInfo.back().hasTurbo;
			speedInfo.crashed = racer.speedInfo.back().crashed;

			if (racer.color != m_Color)
				racer.speedInfo.back().throttle = (m_DragConst * (speedInfo.currentSpeed * exp(m_DragConst / m_MassConst) - racer.speedInfo.back().currentSpeed) / (exp(m_DragConst / m_MassConst) - 1.0));
		}

		racer.speedInfo.push_back(speedInfo);
	}

	return ProcessTick();
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
	std::string color = data["data"]["color"].as<std::string>();
	Racer& racer = GetRacer(color);
	racer.speedInfo.back().crashed = true;

	if (racer.bendInfo.size() > 1)
	{
		if (GetPrevSegment(racer.bendInfo.back().segmentIndex) == racer.bendInfo[racer.bendInfo.size() - 2].segmentIndex)
			racer.bendInfo.pop_back();
	}

	if (racer.bendInfo.size() > 0)
		racer.bendInfo.pop_back();

	if (racer.speedInfo.back().currentSpeed < 10.0)
	{
		double terminalConst = (racer.speedInfo.back().currentSpeed * racer.speedInfo.back().currentSpeed) / m_TrackSegments[racer.speedInfo.back().currentSegment].m_Lanes[racer.speedInfo.back().currentLane].radius;

		if (m_TerminalConst < 0 || m_TerminalConst > terminalConst)
			m_TerminalConst = terminalConst;
	}

	return { make_ping() };
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data)
{
	std::string color = data["data"]["color"].as<std::string>();
	Racer& racer = GetRacer(color);
	racer.speedInfo.back().crashed = false;
	racer.speedInfo.back().hasTurbo = false;

	if (data["data"]["color"] == m_Color)
	{
		m_Throttle = 1.0;
		return { make_throttle(m_Throttle) };
	}

	return { make_ping() };
}

game_logic::msg_vector game_logic::on_lapFinished(const jsoncons::json& data)
{
	std::string color = data["data"]["car"]["color"].as<std::string>();
	Racer& racer = GetRacer(color);
	racer.currentLap = data["data"]["lapTime"]["lap"].as<int>() + 1;
	return { make_throttle(m_Throttle) };
}

game_logic::msg_vector game_logic::on_finish(const jsoncons::json& data)
{
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_tournamentEnd(const jsoncons::json& data)
{
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_turbo_available(const jsoncons::json& data)
{
	for (unsigned int i = 0; i < m_Racers.size(); i++)
		if (!m_Racers[i].speedInfo.back().crashed)
			m_Racers[i].speedInfo.back().hasTurbo = true;

	return { make_ping() };
}

game_logic::msg_vector game_logic::on_turbo_start(const jsoncons::json& data)
{
	std::string color = data["data"]["color"].as<std::string>();
	Racer& racer = GetRacer(color);
	racer.speedInfo.back().hasTurbo = false;

	return { make_throttle(m_Throttle) };
}

game_logic::msg_vector game_logic::on_turbo_end(const jsoncons::json& data)
{
	return { make_throttle(m_Throttle) };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}

void game_logic::AnalyzeTrack()
{
	float bendLength = 0;
	for (int i = (int)m_TrackSegments.size() - 1; i >= 0; i--)
	{
		if (m_TrackSegments[i].m_Radius != 0 && !m_TrackSegments[i].m_Switch)
			bendLength += m_TrackSegments[i].m_Length * (m_TrackSegments[i].m_Angle < 0 ? -1 : 1);
		else if (m_TrackSegments[i].m_Switch && bendLength != 0)
		{
			m_TrackSegments[i].m_SwitchDirection = bendLength < 0 ? -1 : 1;
			bendLength = 0;
		}
	}
}

bool game_logic::UseTurbo()
{
	Racer& racer = GetRacer(m_Color);
	if (racer.speedInfo.back().hasTurbo)
	{
		int lineCount = 0;
		int segment = racer.speedInfo.back().currentSegment;
		
		while (!m_TrackSegments[segment].IsBend())
		{
			segment = GetNextSegment(segment);
			if (++lineCount >= 3)
			{
				return true;
			}
		}
	}

	return false;
}

game_logic::msg_vector game_logic::ProcessTick()
{
	Racer& racer = GetRacer(m_Color);

	if (racer.speedInfo.back().crashed)
		return { make_throttle(m_Throttle) };
	
	unsigned int nextSegment = GetNextSegment(racer.speedInfo.back().currentSegment);
	if (m_SwitchSegment != nextSegment && m_SwitchSegment != racer.speedInfo.back().currentSegment && m_TrackSegments[nextSegment].m_Switch)
	{
		if (m_TrackSegments[nextSegment].m_SwitchDirection != 0)
		{
			m_SwitchSegment = nextSegment;
			return { make_switchLane(m_TrackSegments[nextSegment].m_SwitchDirection < 0 ? "Left" : "Right") };
		}
	}

	if (UseTurbo())
		return { make_turbo() };

	if (gTick == 4)
	{
		m_DragConst = (racer.speedInfo[1].currentSpeed - (racer.speedInfo[2].currentSpeed - racer.speedInfo[1].currentSpeed)) / (racer.speedInfo[1].currentSpeed * racer.speedInfo[1].currentSpeed * racer.speedInfo[1].throttle);
		m_MassConst = 1.0 / (log((racer.speedInfo[3].currentSpeed - (racer.speedInfo[3].throttle / m_DragConst)) / (racer.speedInfo[2].currentSpeed - (racer.speedInfo[3].throttle / m_DragConst))) / -m_DragConst);
	}

	int nextBend = GetNextBend(racer.speedInfo.back().currentSegment);

	if (m_RacerToCopy && (m_SegmentToCopy == GetPrevSegment(racer.speedInfo.back().currentSegment)))
	{
		m_RacerToCopy = NULL;
	}

	if (!m_RacerToCopy)
	{
		if (m_TrackSegments[racer.speedInfo.back().currentSegment].IsBend())
		{
			m_SegmentToCopy = racer.speedInfo.back().currentSegment;
			if (!HasRacerToCopy(m_SegmentToCopy))
			{
				if (HasRacerToCopy(nextBend))
					m_SegmentToCopy = nextBend;
				else
					m_SegmentToCopy = -1;
			}
		}
	}

	if (m_RacerToCopy && (m_SegmentToCopy == racer.speedInfo.back().currentSegment))
	{
		m_Throttle = m_RacerToCopy->speedInfo[m_IndexToCopy++].throttle;
	}
	else
	{
		double bendDistance = m_TrackSegments[racer.speedInfo.back().currentSegment].IsBend() ? 0 : GetDistanceToNextBend(racer);
		double maxSpeed;

		if (m_RacerToCopy)
			maxSpeed = m_RacerToCopy->speedInfo[m_IndexToCopy].currentSpeed;
		else
			maxSpeed = GetMaxSpeedAtRadius(m_TrackSegments[m_TrackSegments[racer.speedInfo.back().currentSegment].IsBend() ? racer.speedInfo.back().currentSegment : nextBend].m_Lanes[racer.speedInfo.back().currentLane].radius);

		if (maxSpeed - racer.speedInfo.back().currentSpeed <= 0.1 && bendDistance >= 0)
		{
			int ticks = GetTicksToSpeed(racer.speedInfo.back().currentSpeed, maxSpeed, m_Throttle);
			double distance = GetDistanceInTicks(racer.speedInfo.back().currentSpeed, racer.speedInfo.back().throttle, ticks);

			if (ticks <= 0 || distance < bendDistance)
			{
				if (maxSpeed > racer.speedInfo.back().currentSpeed)
					m_Throttle = 0.0;
				else
					m_Throttle = maxSpeed / 10.0;
			}
			else
				m_Throttle = 1.0;
		}
		else
			m_Throttle = 1.0;
	}

	if (m_Throttle < 0)
		m_Throttle = 0;
	else if (m_Throttle > 1.0)
		m_Throttle = 1.0;

	return { make_throttle(m_Throttle) };
}

game_logic::Racer& game_logic::GetRacer(std::string& iColor)
{
	for (unsigned int i = 0; i < m_Racers.size(); i++)
	{
		if (m_Racers[i].color == iColor)
			return m_Racers[i];
	}

	return m_Racers.back();
}

unsigned int game_logic::GetNextBend(int iSegment)
{
	for (unsigned int i = iSegment + 1; i < m_TrackSegments.size(); i++)
	{
		if (m_TrackSegments[i].IsBend())
			return i;
	}

	for (unsigned int i = 0; i < m_TrackSegments.size(); i++)
	{
		if (m_TrackSegments[i].IsBend())
			return i;
	}
}

unsigned int game_logic::GetPrevSegment(int iSegment)
{
	return (iSegment == 0) ? (m_TrackSegments.size() - 1) : (iSegment - 1);
}

unsigned int game_logic::GetNextSegment(int iSegment)
{
	return (iSegment == (m_TrackSegments.size() - 1)) ? 0 : (iSegment + 1);
}

double game_logic::GetDistanceToNextSegment(game_logic::Racer& iRacer)
{
	return m_TrackSegments[iRacer.speedInfo.back().currentSegment].m_Lanes[iRacer.speedInfo.back().currentLane].length - iRacer.speedInfo.back().currentDistance;
}

double game_logic::GetDistanceToNextBend(game_logic::Racer& iRacer)
{
	double distance = - iRacer.speedInfo.back().currentDistance;
	bool loop = false;
	int nextBend = GetNextBend(iRacer.speedInfo.back().currentSegment);

	if (iRacer.speedInfo.back().currentSegment > nextBend)
		loop == true;

	for (unsigned int i = iRacer.speedInfo.back().currentSegment; i < (loop ? m_TrackSegments.size() : nextBend); i++)
	{
		distance += m_TrackSegments[i].m_Lanes[iRacer.speedInfo.back().currentLane].length;
	}

	if (loop)
		for (unsigned int i = 0; i < nextBend; i++)
		{
			distance += m_TrackSegments[i].m_Lanes[iRacer.speedInfo.back().currentLane].length;
		}

	return distance;
}

double game_logic::GetMaxSpeedAtRadius(double iRadius)
{
	return (m_TerminalConst < 0) ? 9999 : sqrt(m_TerminalConst * iRadius);
}

double game_logic::CalcNextSpeed(double iSpeed, double iThrottle)
{
	// credits goes to kn3cht for the formula

	return (iSpeed - (iThrottle / m_DragConst)) * exp(- m_DragConst / m_MassConst) + (iThrottle / m_DragConst);
}

int game_logic::GetTicksToSpeed(double iCurrentSpeed, double iTargetSpeed, double iThrottle)
{
	return floor((log((iTargetSpeed - (iThrottle / m_DragConst)) / (iCurrentSpeed - (iThrottle / m_DragConst))) * m_MassConst) / (-m_DragConst));
}

double game_logic::GetDistanceInTicks(double iCurrentSpeed, double iThrottle, int iTicks)
{
	if (iTicks < 0)
		return 0;

	return (m_MassConst / m_DragConst) * (iCurrentSpeed - (iThrottle / m_DragConst)) * (1.0 - exp((-m_DragConst * iTicks) / m_DragConst)) + (iThrottle / m_DragConst) * iTicks;
}

bool game_logic::HasRacerToCopy(int iBendIndex)
{
	m_IndexToCopy = -1;

	int bestIndex = 0;
	int bestMemory = 0;
	int myIndex;

	for (unsigned int i = 0; i < m_Racers.size(); i++)
	{
		if (m_Racers[i].color == m_Color)
			myIndex = i;

		for (unsigned int j = 0; j < m_Racers[i].bendInfo.size(); j++)
		{
			if (m_Racers[i].bendInfo[j].firstTick > -1 && m_Racers[i].bendInfo[j].lastTick > -1 && m_Racers[i].bendInfo[j].segmentIndex == iBendIndex && m_Racers[i].bendInfo[j].elapsedTicks() < m_Racers[bestIndex].bendInfo[bestMemory].elapsedTicks())
			{
				bestIndex = i;
				bestMemory = j;
				m_IndexToCopy = m_Racers[bestIndex].bendInfo[bestMemory].firstTick;
			}
		}
	}

	if (m_IndexToCopy > -1 && myIndex != bestIndex)
	{
		m_RacerToCopy = &m_Racers[bestIndex];
		return true;
	}

	return false;
}