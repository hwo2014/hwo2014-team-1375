#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>

class game_logic
{
private:

	struct Lane
	{
		int m_Index;
		double m_DistanceFromCenter;

		Lane() {}

		Lane(int iIndex, double iDistance)
		{
			m_Index = iIndex;
			m_DistanceFromCenter = iDistance;
		}
	};

	struct LaneInfo
	{
		double length, radius, angle;

		LaneInfo()
		{
			length = radius = angle = 0;
		}

		LaneInfo(double iAngle, double iRadius, double iLength)
		{
			angle = iAngle;
			radius = iRadius;
			length = iLength;
		}
	};

	struct Segment
	{
		double m_Length;
		double m_Radius;
		double m_Angle;
		bool m_Switch;
		int m_SwitchDirection;

		std::vector<LaneInfo> m_Lanes;

		Segment(double iLength, double iRadius, double iAngle, bool iSwitch)
		{
			m_Length = iLength;
			m_Radius = iRadius;
			m_Angle = iAngle;
			m_Switch = iSwitch;
			m_SwitchDirection = 0;
		}

		bool IsBend()
		{
			return (m_Angle != 0 && m_Radius != 0);
		}
	};

	struct Car
	{
		double m_Length;
		double m_Width;
		double m_GuideFlagPosition;
	};

	struct SpeedInfo2
	{
		double throttle;
		double currentSpeed, currentAngle, currentDistance; // car specific
		double deltaSpeed, deltaAngle; // change in this frame
		int currentSegment, currentLane;
		bool hasTurbo, crashed, changingLane;

		SpeedInfo2()
		{
			currentSpeed = currentAngle = currentDistance = deltaSpeed = deltaAngle = 0;
			currentSegment = currentLane = -1;
			hasTurbo = crashed = changingLane = false;
		}
	};

	struct BendInfo2
	{
		int segmentIndex, firstTick, lastTick;

		BendInfo2()
		{
			segmentIndex = firstTick = lastTick = -1;
		}
		
		int elapsedTicks()
		{
			return (lastTick - firstTick);
		}
	};

	struct Racer
	{
		std::string color;
		std::vector<SpeedInfo2> speedInfo;
		std::vector<BendInfo2> bendInfo;
		int currentLap;

		Racer(std::string iColor)
		{
			color = iColor;
		}
	};

	Car m_Car;
	double m_Throttle;
	int m_SwitchSegment;
	std::string m_Color;
	Racer* m_RacerToCopy;
	int m_IndexToCopy;
	int m_SegmentToCopy;

	std::vector<Segment> m_TrackSegments;
	std::vector<Racer> m_Racers;
	std::vector<Lane> m_Lanes;

	double m_MassConst, m_DragConst, m_TerminalConst;

public:
  typedef std::vector<jsoncons::json> msg_vector;

  game_logic();
  msg_vector react(const jsoncons::json& msg);

private:
  typedef std::function<msg_vector(game_logic*, const jsoncons::json&)> action_fun;
  const std::map<std::string, action_fun> action_map;

  msg_vector on_join(const jsoncons::json& data);
  msg_vector on_yourCar(const jsoncons::json& data);
  msg_vector on_game_init(const jsoncons::json& data);
  msg_vector on_game_start(const jsoncons::json& data);
  msg_vector on_car_positions(const jsoncons::json& data);
  msg_vector on_crash(const jsoncons::json& data);
  msg_vector on_spawn(const jsoncons::json& data);
  msg_vector on_lapFinished(const jsoncons::json& data);
  msg_vector on_finish(const jsoncons::json& data);
  msg_vector on_tournamentEnd(const jsoncons::json& data);
  msg_vector on_game_end(const jsoncons::json& data);
  msg_vector on_turbo_available(const jsoncons::json& data);
  msg_vector on_turbo_start(const jsoncons::json& data);
  msg_vector on_turbo_end(const jsoncons::json& data);
  msg_vector on_error(const jsoncons::json& data);
  msg_vector ProcessTick();

  void AnalyzeTrack();
  bool UseTurbo();

  Racer& GetRacer(std::string& iColor);
  unsigned int GetNextBend(int iSegment);
  unsigned int GetPrevSegment(int iSegment);
  unsigned int GetNextSegment(int iSegment);
  unsigned int GetCurrentLaneIndex();

  double GetDistanceToNextSegment(game_logic::Racer& iRacer);
  double GetDistanceToNextBend(game_logic::Racer& iRacer);
  double GetMaxSpeedAtRadius(double iRadius);
  double CalcNextSpeed(double iSpeed, double iThrottle);
  int GetTicksToSpeed(double iCurrentSpeed, double iTargetSpeed, double iThrottle);
  double GetDistanceInTicks(double iCurrentSpeed, double iThrottle, int iTicks);
  bool HasRacerToCopy(int iBendIndex);
};

#endif
