#include "protocol.h"

extern unsigned int gTick;

namespace hwo_protocol
{
  jsoncons::json make_request(const std::string& msg_type, const jsoncons::json& data)
  {
    jsoncons::json r;
    r["msgType"] = msg_type;
    r["data"] = data;
	r["gameTick"] = gTick;
    return r;
  }

  jsoncons::json make_join(const std::string& name, const std::string& key)
  {
    jsoncons::json data;
    data["name"] = name;
    data["key"] = key;
    return make_request("join", data);
  }

  jsoncons::json make_ping()
  {
    return make_request("ping", jsoncons::null_type());
  }

  jsoncons::json make_disconnect()
  {
	  return make_request("disconnect", jsoncons::null_type());
  }

  jsoncons::json make_throttle(double throttle)
  {
    return make_request("throttle", throttle);
  }

  jsoncons::json make_switchLane(const char* direction)
  {
	  return make_request("switchLane", direction);
  }

  jsoncons::json make_turbo()
  {
	  return make_request("turbo", "For the Horde!");
  }

}  // namespace hwo_protocol
